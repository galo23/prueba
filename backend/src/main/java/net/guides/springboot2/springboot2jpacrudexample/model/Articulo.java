package net.guides.springboot2.springboot2jpacrudexample.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "articulo")
public class Articulo {
	
	
	private long codigoArticulo;
	private long idProveedor;
	private String nombreArticulo;
	private String caracteristicas;
	
	
	@Id
	@Column(name = "codigoArticulo", nullable = false)
	public long getCodigoArticulo() {
		return codigoArticulo;
	}
	public void setCodigoArticulo(long codigoArticulo) {
		this.codigoArticulo = codigoArticulo;
	}
	
	@Column(name = "idProveedor", nullable = false)
	public long getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(long idProveedor) {
		this.idProveedor = idProveedor;
	}
	
	@Column(name = "nombreArticulo", nullable = false)
	public String getNombreArticulo() {
		return nombreArticulo;
	}
	public void setNombreArticulo(String nombreArticulo) {
		this.nombreArticulo = nombreArticulo;
	}	
	
	@Column(name = "caracteristicas", nullable = false)
	public String getCaracteristicas() {
		return caracteristicas;
	}
	public void setCaracteristicas(String caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

	
	
	
	
	
	
	
	
	
	
	
	
}
