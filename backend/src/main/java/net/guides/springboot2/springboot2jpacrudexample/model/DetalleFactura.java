package net.guides.springboot2.springboot2jpacrudexample.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.ForeignKey;

@Entity
@Table(name = "detalle_factura")
public class DetalleFactura {
	
	private long numeroDetalleFactura;
	private Factura numeroFactura;
	private String codigoArticulo;
	private String cantidad;
	private String porcentaje;
	
	
	public DetalleFactura() {		
	}

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getNumeroDetalleFactura() {
		return numeroDetalleFactura;
	}
	public void setNumeroDetalleFactura(long numeroDetalleFactura) {
		this.numeroDetalleFactura = numeroDetalleFactura;
	}

	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "numeroFactura", nullable = false, foreignKey = @ForeignKey(name = "NUM_FACTURA_FK"))
	public Factura getNumeroFactura() {
		return numeroFactura;
	}
	public void setNumeroFactura(Factura numeroFactura) {
		this.numeroFactura = numeroFactura;
	}
		
	@Column(name = "codigoArticulo", nullable = false)
	public String getCodigoArticulo() {
		return codigoArticulo;
	}
	public void setCodigoArticulo(String codigoArticulo) {
		this.codigoArticulo = codigoArticulo;
	}

	@Column(name = "cantidad", nullable = false)
	public String getCantidad() {
	return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	@Column(name = "porcentaje", nullable = false)
	public String getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(String porcentaje) {
		this.porcentaje = porcentaje;
	}
	
	
	
	
	
}
