package net.guides.springboot2.springboot2jpacrudexample.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "factura")
public class Factura {
	
	private long numeroFactura;
	private Date fechaFactura;
	
	
	public Factura() {		
	}


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getNumeroFactura() {
		return numeroFactura;
	}
	public void setNumeroFactura(long numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	@Column(name = "fecha", nullable = false)
	public Date getFechaFactura() {
		return fechaFactura;
	}

	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}
	
	
	
	
	

}
