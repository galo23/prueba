package net.guides.springboot2.springboot2jpacrudexample.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.guides.springboot2.springboot2jpacrudexample.model.Factura;
import net.guides.springboot2.springboot2jpacrudexample.repository.FacturaRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class FacturaController {
	
	@Autowired
	private FacturaRepository facturaRepository;
	
	
	@PostMapping("/facturas")
	public Factura createDetalleFacturas(@Valid @RequestBody Factura factura) {		
		return facturaRepository.save(factura);
	}

}
