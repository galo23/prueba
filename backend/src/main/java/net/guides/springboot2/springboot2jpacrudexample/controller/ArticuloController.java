package net.guides.springboot2.springboot2jpacrudexample.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.guides.springboot2.springboot2jpacrudexample.model.Articulo;
import net.guides.springboot2.springboot2jpacrudexample.repository.ArticuloRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class ArticuloController {
	
	@Autowired
	private ArticuloRepository articuloRepository;
	
	
	@GetMapping("/articulo")
	public List<Articulo> getAllArticulos() {
		return articuloRepository.findAll();
	}
	@PostMapping("/articulo")
	public Articulo createArticulo(@Valid @RequestBody Articulo proveedor) {
		return articuloRepository.save(proveedor);
	}

}
