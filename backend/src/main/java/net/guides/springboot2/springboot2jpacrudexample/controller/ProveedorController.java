package net.guides.springboot2.springboot2jpacrudexample.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.guides.springboot2.springboot2jpacrudexample.exception.ResourceNotFoundException;
import net.guides.springboot2.springboot2jpacrudexample.model.Proveedor;
import net.guides.springboot2.springboot2jpacrudexample.repository.ProveedorRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class ProveedorController {
	
	@Autowired
	private ProveedorRepository proveedorRepository;
	
	
	@GetMapping("/proveedor")
	public List<Proveedor> getAllProveedores() {
		return proveedorRepository.findAll();
	}

	@GetMapping("/proveedor/{id}")
	public ResponseEntity<Proveedor> getProveedorById(@PathVariable(value = "id") Long proveedorId)
			throws ResourceNotFoundException {
		Proveedor employee = proveedorRepository.findById(proveedorId)
				.orElseThrow(() -> new ResourceNotFoundException("Id no encontrado :: " + proveedorId));
		return ResponseEntity.ok().body(employee);
	}

	@PostMapping("/proveedor")
	public Proveedor createProveedor(@Valid @RequestBody Proveedor proveedor) {
		return proveedorRepository.save(proveedor);
	}

}
