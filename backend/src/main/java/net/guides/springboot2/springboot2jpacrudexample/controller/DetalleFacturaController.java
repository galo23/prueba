package net.guides.springboot2.springboot2jpacrudexample.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.guides.springboot2.springboot2jpacrudexample.model.DetalleFactura;
import net.guides.springboot2.springboot2jpacrudexample.model.Factura;
import net.guides.springboot2.springboot2jpacrudexample.repository.DetalleFacturaRepository;
import net.guides.springboot2.springboot2jpacrudexample.repository.FacturaRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class DetalleFacturaController {
	
	@Autowired
	private DetalleFacturaRepository detalleFacturaRepository;
	@Autowired
	private FacturaRepository facturaRepository;
	
	
	@GetMapping("/detalleFacturas")
	public List<DetalleFactura> getAllDetalleFacturas() {
		return detalleFacturaRepository.findAll();
	}
	
	
	@PostMapping("/detalleFacturas")
	public DetalleFactura createDetalleFacturas(@Valid @RequestBody DetalleFactura detalleFactura) {
		
		Factura factura = facturaRepository.save(detalleFactura.getNumeroFactura());
		detalleFactura.getNumeroFactura().setNumeroFactura(factura.getNumeroFactura());
		
		return detalleFacturaRepository.save(detalleFactura);
	}

}
