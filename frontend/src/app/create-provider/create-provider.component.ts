import { ProveedorService } from '../proveedor.service';
import { Proveedor } from '../proveedor';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-provider',
  templateUrl: './create-provider.component.html',
  styleUrls: ['./create-provider.component.css']
})
export class CreateProviderComponent implements OnInit {

  proveedor: Proveedor = new Proveedor();
  submitted = false;

  constructor(private proveedorService: ProveedorService,
    private router: Router) { }

  ngOnInit() {
  }

  newProveedor(): void {
    this.submitted = false;
    this.proveedor = new Proveedor();
  }

  save() {
    this.proveedorService
    .createProveedor(this.proveedor).subscribe(data => {
      console.log(data)
      this.proveedor = new Proveedor();
      this.gotoList();
    }, 
    error => console.log(error));
  }

  onSubmit() {
    this.submitted = true;
    this.save();    
  }

  gotoList() {
    this.router.navigate(['/employees']);
  }
}
