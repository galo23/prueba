import { FacturaService } from '../factura.service';
import { detalleFactura } from '../detalleFactura';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create-bill',
  templateUrl: './create-bill.component.html',
  styleUrls: ['./create-bill.component.css']
})
export class CreateBillComponent implements OnInit {

  factura: detalleFactura = new detalleFactura();  
  submitted = false;
  id: number;

  constructor(private route: ActivatedRoute, private facturaService: FacturaService,
    private router: Router) { }

  ngOnInit() {

    this.id = this.route.snapshot.params['id'];
  }


  newFactura(): void {
    this.submitted = false;
    this.factura = new detalleFactura();
  }

  save() {
    this.facturaService
    .createFactura(this.factura).subscribe(data => {
      console.log(data)
      this.factura = new detalleFactura();
      this.gotoList();
    }, 
    error => console.log(error));
  }

  onSubmit() {
    this.submitted = true;
    this.save();    
  }

  gotoList() {
    this.router.navigate(['/employees']);
  }




}
