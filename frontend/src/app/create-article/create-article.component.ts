import { ArticuloService } from '../articulo.service';
import { Articulo } from '../articulo';
import { Proveedor } from '../proveedor';
import { Observable } from "rxjs";
import { ProveedorService } from "../proveedor.service"
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.css']
})
export class CreateArticleComponent implements OnInit {

  articulo: Articulo = new Articulo();
  proveedores: Observable<Proveedor[]>;
  submitted = false;

 constructor(private articuloService: ArticuloService, private proveedorService: ProveedorService, 
 private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    console.log("entra a recargar");
    this.proveedores = this.proveedorService.getProveedorList();
    console.log(this.proveedores);
  }


  newArticulo(): void {
    this.submitted = false;
    this.articulo = new Articulo();
  }

  save() {
    this.articuloService
    .createArticulo(this.articulo).subscribe(data => {
      console.log(data)
      this.articulo = new Articulo();
      this.gotoList();
    }, 
    error => console.log(error));
  }

  onSubmit() {
    this.submitted = true;
    this.save();    
  }

  gotoList() {
    this.router.navigate(['/employees']);
  }
}
