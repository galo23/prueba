export class Articulo {
  codigoArticulo: number;
  idProveedor: number;
  nombreArticulo: string;
  caracteristicas: string;
}

