export class Employee {
  id: number;
  nombre: string;
  apellido1: string;
  apellido2: string;
  nombreEmpresa: string;
  direccion: string;
  ciudad: string;
  cp: string;
  provincia: string;
  pais: string;
}

