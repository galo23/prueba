import { Factura } from './factura';

export class detalleFactura {
  numeroDetalleFactura: number;
  facturaObj: Factura = new Factura();
  codigoArticulo: string;
  cantidad: string;
  porcentaje: string;  
}

