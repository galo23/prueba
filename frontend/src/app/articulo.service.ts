import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArticuloService {

  private baseUrl = 'http://localhost:8080/springboot-crud-rest/api/v1/articulo';

  constructor(private http: HttpClient) { }

  getArticulo(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createArticulo(articulo: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, articulo);
  }

  getArticuloList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
