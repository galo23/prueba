import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Proveedor } from './proveedor';

@Injectable({
  providedIn: 'root'
})
export class ProveedorService {

  private baseUrl = 'http://localhost:8080/springboot-crud-rest/api/v1/proveedor';
 
  constructor(private http: HttpClient) { }

  getProveedor(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createProveedor(proveedor: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, proveedor);
  }

  getProveedorList(): Observable<any> {     
    return this.http.get(`${this.baseUrl}`);
  }
}
