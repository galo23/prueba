import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FacturaService {

  private baseUrl = 'http://localhost:8080/springboot-crud-rest/api/v1/detalleFacturas';

  constructor(private http: HttpClient) { }

  getFactura(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createFactura(factura: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, factura);
  }

  
  getFacturasList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
